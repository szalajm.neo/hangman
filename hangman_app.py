def hangman_graphins(number_of_errors):
    if number_of_errors == 0:
        print("\U0001F642	")
    elif number_of_errors == 1:
        print("===")
    elif number_of_errors == 2:
        print(" |\n |\n |\n |\n |\n |\n |\n===")
    elif number_of_errors == 3:
        print(" ________\n |\n |\n |\n |\n |\n |\n |\n===")
    elif number_of_errors == 4:
        print(" ________\n |      |\n |      O\n |     /|\\\n |\n |\n |\n |\n=== ")
    elif number_of_errors == 5:
        print(" ________\n |      |\n |      O\n |     /|\\\n |     / \\\n |\n |\n |\n=== ")
    else: 
        print("Please enter a number from 0 through 5")

secret_word = input("Enter your secret word: ").lower()

result = "$" * len(secret_word)
result_list = list(result)

number_of_misguesses = 0
number_of_letters_guessed = 0

while number_of_misguesses < 5 and number_of_letters_guessed < len(secret_word):
    guess = input("Guess a letter: ").lower()
    print(guess)
    positions = []  
    for index, char in enumerate(secret_word):
        if char == guess:
            positions.append(index)
    if len(positions) > 0:
        for i in positions:
            result_list[i] = guess
        number_of_letters_guessed += len(positions)
        print("You guessed it right!")
        print(''.join(result_list))
    else:
        number_of_misguesses += 1
        print(f"You were wrong! That's your {str(number_of_misguesses)} error")
        hangman_graphins(number_of_misguesses)
        
if number_of_letters_guessed == len(secret_word):
    print(f"Congratulations, you guessed the secret word! It was {secret_word}")
else: 
    print("You lost, hangman!")
        
    